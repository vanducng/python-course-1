import numpy as np

def arr_random(M, N, fr, to, type='f'):
    arr = np.array(np.random.rand(M, N)) # random float element from 0->1
    for i in range(M):
        for j in range(N):
            arr[i,j] *= np.random.randint(fr, to)
    return arr.astype(type)

arr = arr_random(10, 8, -10, 10)
print("Random matrice:\n", arr)
num = float(input("input 1 random float number, num = "))


temp = abs(arr - num)
idx_1stClosest = np.argmin(temp)

print("1st closest number from array is ", np.ravel(arr)[idx_1stClosest])

np.ravel(temp)[idx_1stClosest] = np.max(np.ravel(temp))
idx_2ndClosest = np.argmin(temp)
print("2nd closest number from array is ", np.ravel(arr)[idx_2ndClosest])

np.ravel(temp)[idx_2ndClosest] = np.max(np.ravel(temp))
idx_3rdClosest = np.argmin(temp)
print("3rd closest number from array is ", np.ravel(arr)[idx_3rdClosest])