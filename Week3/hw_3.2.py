import numpy as np
import random as rd

N = int(input("N = "))

arr = np.array(np.random.rand(N,N,N))*10
print("Matrix = \n", arr)

for i in range(3):
    print("""\
Min Axis{0}:\n {1}
Max Axis{0}:\n {2}
Sum Axis{0}:\n {3}
    """.format(i, arr.min(axis=i), arr.max(axis=i), arr.sum(axis=i)))
