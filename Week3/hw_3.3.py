import numpy as np
import random as rd

def arr_random(M, N, K, fr, to, type='f'):
    arr = np.array(np.random.rand(M, N, K)) # random float element from 0->1
    for i in range(M):
        for j in range(N):
            for k in range(K):
                arr[i,j,k] *= np.random.randint(fr, to)
    return arr.astype(type)

M = int(input("M = "))
N = int(input("N = "))
K = int(input("K = "))

arr = arr_random(M, N, K, -100, 100, 'i')
print("Matrix = \n", arr, "\n")

for i in range(3):
    print("""\
Min Axis{0}:\n {1}
Max Axis{0}:\n {2}
Sum Axis{0}:\n {3}
    """.format(i, arr.min(axis=i), arr.max(axis=i), arr.sum(axis=i)))