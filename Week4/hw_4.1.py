import pandas as pd

def countvowel(S):
    temp=''.join(c for c in S if c in 'aeiou')
    return len(temp)

S = "Ah meta descriptions... the last bastion of traditional marketing! \
The only cross-over point between marketing and search engine optimisation! \
The knife edge between beautiful branding and an online suicide note!"

S_refined = ''.join(c for c in S if c not in '~!@#$%^&*().')

L = S_refined.split(' ')

S_Series = pd.Series(L, name="steps")

S_Series_1 = S_Series.map(lambda x: countvowel(x)>2) #Create Mask

S_Series_2 = S_Series.where(S_Series_1).dropna()

#print(S_refined)
#print(S_Series)
print(S_Series_2)                   #print words with at least 3 vowels
print(pd.DataFrame(L)[S_Series_1])  #print words with at least 3 vowels with dataframe