import pandas as pd

filepath = './Data/Iris_Data.csv'
data = pd.read_csv(filepath)

#Calculate the statistical description of sepal_length, sepal_width, petal_length and petal_width by type of flower
data.groupby('species').describe().T

#normalize its sepal_length and petal_length
df_mean_by_type          = data.groupby('species').mean()

df_sepal_length          = df_mean_by_type['sepal_length'][data.species]
df_sepal_length.index    = range(len(data.sepal_length))



df_petal_length          = df_mean_by_type['petal_length'][data.species]
df_petal_length.index    = range(len(data.petal_length))

data['sepal_length_normalize'] = (data.sepal_length - df_sepal_length)/2
data['petal_length_normalize'] = (data.petal_length - df_petal_length)/2

print(data)