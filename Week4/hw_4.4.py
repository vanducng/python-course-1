import pandas as pd
import numpy as np
import random

arr_rand = np.array(range(20))

random.shuffle(arr_rand)
S1 = pd.Series('id' + str(x) for x in arr_rand[0:10])
S1.name = "key"

random.shuffle(arr_rand)
S2 = pd.Series('id' + str(x) for x in arr_rand[0:10])
S2.name = "key"

S3 = pd.Series(np.random.random_integers(-100,101,10))
S3.name="data1"
S4 = pd.Series(np.random.random_integers(-100,101,10))
S4.name="data2"

df1 = pd.DataFrame(data=pd.concat([S1, S3], axis=1))
df2 = pd.DataFrame(data=pd.concat([S2, S4], axis=1))

df3=pd.merge(df1, df2, how='inner', left_on='key', right_on='key')

print(df1, '\n\n', df2, '\n\n', df3)