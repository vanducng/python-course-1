import numpy as np
from PIL import Image

k = np.array([[-1,-1,-1],[-1,8,-1],[-1,-1,-1]], dtype=np.int8)
img=Image.open('table.jpg')
img_rgb=np.array(img, dtype=np.uint16)
img_grey=(img_rgb[:,:,:1] + img_rgb[:,:,1:2] + img_rgb[:,:,2:3])/3

h,w,d=img_grey.shape

r=np.zeros((h,w))

for i in range(h-2):
    for j in range(w-2):
        r[i+1,j+1]=max(0, np.sum(img_grey[i:i+3,j:j+3]*k))

simg=Image.fromarray(r.astype(np.uint8))
simg.save("new_grey.png")
