import pandas as pd
import numpy as np

#Generate dataframe
sale = np.random.randint(10, 30, 50)
df = pd.DataFrame(data=sale, columns=["sale"], index = pd.date_range(start='2018-01-31', periods=50, name="month", freq='M'))
print(df)


#calculate sales by year
df1 = df.groupby(df.index.year).sum()
df1 = df1.reindex(df1.index.rename('Year'))
print(df1)

