def sum(x, y):
    sum = x + y
    if sum in range (15, 20):
        return 20
    else:
        return sum

print(sum(10, 6)) # sum = 16 which is between 15-20, then result will be 20
print(sum(10, 2)) # sum = 12 which is out of range 15-20, then result will be 10+2 = 12
print(sum(10, 12)) # sum = 22 which is out of range 15-20, then result will be 10+12 = 22