userinput = input("Please input the list of number with separator ',' : ")
even_list = []
odd_list = []
try:
    input_list = userinput.split(",")
    print(input_list)
    for el in input_list:
        tmp = int(el)
        if (tmp%2==0):
            even_list.append(tmp)
        else:
            odd_list.append(tmp)

    print("even list = {0} \nOdd list = {1}".format(even_list, odd_list))
except:
    print("error")