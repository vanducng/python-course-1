import numpy as np
import matplotlib.pyplot as plt
from collections import Counter
import math

def histogram(L):
    Dict = {}
    for i in L:
        key_existed = False
        for j in Dict:
            if i == j:
                Dict[j] += 1
                key_existed = True
                break
        if not key_existed:
            Dict[i] = 1

    return Dict

def max_list(L):
    max = 0
    for i in L:
        if max < i:
            max = i
    return max

def min_list(L):
    min = 10
    for i in L:
        if min > i:
            min = i
    return min

def variance_list(L):
    n = len(L)
    mean = sum(L)/n
    X = 0.0
    for i in L:
        X+= (i - mean)**2
    return X/n


N = input("N = ")
l=[]
for i in range(int(N)):
    l.append(np.random.randint(0,10))

#Create and print a histogram of the list
print(l)
print(histogram(l))
#print(Counter(l))
# plt.bar(list(histogram(l).keys()), histogram(l).values(), color='b') #visualize by matplot.
# plt.show()


# Manually calculate and print the statistics description of the list
max_ = max_list(l)
min_ = min_list(l)
variance = variance_list(l)
st_dev = math.sqrt(variance)

print(max_, min_, variance, st_dev)
print(max(l), min(l), np.var(l), np.std(l))


# Remove duplicates from the list
print(set(l))